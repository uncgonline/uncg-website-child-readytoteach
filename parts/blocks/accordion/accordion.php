<?php

/**
 * Accordion Block Template.
 *
 * @param  array  $block  The block settings and attributes.
 * @param  bool  $is_preview  True during AJAX preview.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'accordion-'.$block['id'];
if ( ! empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'accordion-block';
if ( ! empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if ( ! empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// if in preview mode and acf field does not have a value, show placeholder
if ($is_preview && ! have_rows('accordion_block'))  :
    get_template_part('parts/blocks/preview', 'placeholder', ['title' => 'Accordion Placeholder']);
else: ?>

    <section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
        <?php

        $accordion_iter  = 0;
        $content_iter    = $id.'_card';
        $first_card_open = get_field('show_first_card');
        ?>

        <div id="accordion_<?php echo $content_iter; ?>">
            <?php
            // loop through the rows of data
            while (have_rows('accordion_block')) : the_row(); ?>

                <div class="accordion-card">
                    <div id="heading_<?php echo $content_iter; ?>_<?php echo $accordion_iter; ?>">
                        <button
                                class="accordion-btn"
                                data-toggle="collapse"
                                data-target="#collapse_<?php echo $content_iter; ?>_<?php echo $accordion_iter; ?>"
                                aria-expanded="<?php echo ($accordion_iter === 0 && $first_card_open) ? "true" : "false"; ?>"
                                aria-controls="collapse_<?php echo $content_iter; ?>_<?php echo $accordion_iter; ?>"
                        >
                            <?php the_sub_field('title'); ?>
                        </button>
                    </div>

                    <div
                            id="collapse_<?php echo $content_iter; ?>_<?php echo $accordion_iter; ?>"
                            class="collapse <?php if ($accordion_iter === 0 && $first_card_open) {
                                echo "in";
                            } ?>"
                            aria-labelledby="heading_<?php echo $content_iter; ?>_<?php echo $accordion_iter; ?>"
                            data-parent="#accordion_<?php echo $content_iter; ?>"
                    >
                        <div class="accordion-card-body">
                            <?php the_sub_field('text'); ?>
                            <InnerBlocks />
                        </div>
                    </div>
                </div>
                <?php
                $accordion_iter++;
            endwhile;
            ?>
        </div>
    </section>
<?php
endif;
//while (have_rows('accordion_block')) : the_row();
//
//    echo '<div class="block-about__inner">';
//    echo '<div class="block-about__content">';
//    echo 'inner blocks start';
//    echo '<InnerBlocks />';
//    echo 'inner blocks end';
//    echo '</div>';
//    echo '<div class="block-about__image">';
//    echo ( get_sub_field('title') );
//    echo '</div>';
//    echo '</div>';
//endwhile;
//echo '</div>';
