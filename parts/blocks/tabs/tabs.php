<?php

/**
 * Tabs Block Template.
 *
 * @param  array  $block  The block settings and attributes.
 * @param  bool  $is_preview  True during AJAX preview.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'tabs-'.$block['id'];
if ( ! empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'tabs-block';
if ( ! empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if ( ! empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// if in preview mode and acf field does not have a value, show placeholder
if ($is_preview && ! have_rows('tabs_block'))  :
    get_template_part('parts/blocks/preview', 'placeholder', ['title' => 'Tabs Placeholder']);
else: ?>

    <section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
        <?php

        $content_iter = $id.'_tabs';
        ?>

        <div id="tabs_<?php echo $content_iter; ?>">
            <?php
            // set up tab controls
            if (have_rows('tabs_block')) :
                $tabs_iter = 0;
                $selected = 'true';
                $active = ' active';
                ?>
                <ul class="nav nav-tabs" role="tablist">
                    <?php
                    // loop through the rows of data
                    while (have_rows('tabs_block')) : the_row(); ?>


                        <li class="nav-item<?php echo $active; ?>">
                            <a class="nav-link<?php echo $active; ?>"
                               id="tab_<?php echo $content_iter.'-'.$tabs_iter; ?>-tab"
                               data-toggle="tab" href="#tab_<?php echo $content_iter.'-'.$tabs_iter; ?>"
                               aria-controls="tab_<?php echo $content_iter.'-'.$tabs_iter; ?>"
                               aria-selected="<?php echo $selected ?>">
                                <?php the_sub_field('sub_field_tab_title'); ?>
                            </a>
                        </li>
                        <?php
                        $selected = 'false';
                        $active   = '';
                        $show     = '';
                        $tabs_iter++;
                    endwhile; ?>
                </ul>

                <?php
                // add tab content
                $tabs_iter = 0;
                $show      = ' active';
                $fade = ' in'
                ?>
                <div class="tab-content">

                    <?php while (have_rows('tabs_block')) : the_row(); ?>
                        <div class="tab-pane fade<?php echo $show.$fade; ?>"
                             id="tab_<?php echo $content_iter.'-'.$tabs_iter; ?>"
                             role="tabpanel"
                             aria-labeledbby="tab_<?php echo $content_iter.'-'.$tabs_iter; ?>-tab">
                            <?php the_sub_field('sub_field_tab_content'); ?>
                        </div>
                        <?php
                        $show = '';
                        $tabs_iter++;
                    endwhile; ?>

                </div><!-- tab-content -->
            <?php endif;

            ?>

        </div>
    </section>
<?php endif;
