<?php

/**
 * Chapter Box Template.
 *
 * @param  array  $block  The block settings and attributes.
 * @param  bool  $is_preview  True during AJAX preview.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'chapter-box-'.$block['id'];
if ( ! empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'chapter-box-block';
if ( ! empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if ( ! empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// if in preview mode and acf field does not have a value, show placeholder
if ($is_preview && ! have_rows('chapter-box_block'))  :
    get_template_part('parts/blocks/preview', 'placeholder', ['title' => 'Chapter Box Placeholder']);
else: ?>

    <section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
        <?php
        $chapter_box_iter = 0;
        $content_iter     = $id.'_chapter-box';
        ?>

        <?php
        // loop through the rows of data
        while (have_rows('chapter_box_block')) : the_row(); ?>
            <a id="<?php echo $content_iter; ?>" class="chapter-box" href="
                    <?php $link = get_sub_field('sub_field_chapter_link');
            if ( ! empty($link)):
                echo $link;
            endif; ?>
                    ">
                <div class="chapter-box-image">
                    <?php $image = get_sub_field('sub_field_chapter_image');
                    if ( ! empty($image)): ?>
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>"/>
                    <?php endif; ?>
                </div>
                <div class="chapter-box-title">
                    <?php $title = get_sub_field('sub_field_chapter_title');
                    if ($title):
                        echo $title;
                    endif;
                    ?>
                    <svg xmlns="http://www.w3.org/2000/svg" width="10.799" height="10.525"
                         viewBox="0 0 10.799 10.525">
                        <path class="a"
                              d="M4.591,3.352l.535-.535a.576.576,0,0,1,.817,0L10.629,7.5a.576.576,0,0,1,0,.817L5.944,13a.576.576,0,0,1-.817,0l-.535-.535a.579.579,0,0,1,.01-.827l2.9-2.767H.578A.577.577,0,0,1,0,8.3V7.524a.577.577,0,0,1,.578-.578H7.505L4.6,4.179A.575.575,0,0,1,4.591,3.352Z"
                              transform="translate(0 -2.647)"/>
                    </svg>
                </div>
                <div class="chapter-box-description">
                    <?php $description = get_sub_field('sub_field_chapter_description');
                    if ($description):
                        echo $description;
                    endif;
                    ?>
                </div>
            </a>
            <?php
            $chapter_box_iter++;
        endwhile;
        ?>
    </section>
<?php
endif;
