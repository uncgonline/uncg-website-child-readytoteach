<?php

/**
 * placeholder for when no acf fields are present for a gutenberg block
 * accepts two optional arguments for the title ($args['title']) and text ($args['text'])
 */
?>
<div class="acf-preview-placeholder components-placeholder block-editor-media-placeholder is-large">
    <div class="components-placeholder__label">
        <?php echo isset($args['title']) ? $args['title'] : 'Placeholder'; ?>
    </div>
    <div class="components-placeholder__instructions">
        <?php echo isset($args['text']) ? $args['text'] : 'Click <span class="dashicons dashicons-edit"></span><span class="screen-reader-text">edit icon</span> to edit.'; ?>
    </div>
</div>
