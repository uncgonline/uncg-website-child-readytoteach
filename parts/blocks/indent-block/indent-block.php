<?php

/**
 * Indent Block Template.
 *
 * @param  array  $block  The block settings and attributes.
 * @param  bool  $is_preview  True during AJAX preview.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'indent-block-'.$block['id'];
if ( ! empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'indent-block-block';
if ( ! empty($block['className'])) {
    $className .= ' '.$block['className'];
}
if ( ! empty($block['align'])) {
    $className .= ' align'.$block['align'];
}

// if in preview mode and acf field does not have a value, show placeholder
if ($is_preview && ! have_rows('indent_block'))  :
    get_template_part('parts/blocks/preview', 'placeholder', ['title' => 'Indent Block Placeholder']);
else: ?>

    <section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
        <?php

        $indent_block_iter = 0;
        $content_iter      = $id;

        while (have_rows('indent_block')) : the_row(); ?>

            <?php
            $icon_name = get_sub_field('sub_field_indent_icon');
            ?>

            <div class="indent-block indent-block-<?php echo $icon_name; ?>" id="<?php echo $content_iter; ?>">

                <?php if ($icon_name !== 'none'): ?>

                    <div id="icon_<?php echo $content_iter; ?>_<?php echo $indent_block_iter; ?>"
                         class="indent-block-icon indent-block-icon-<?php echo $icon_name; ?>">
                        <?php get_template_part('parts/svg/'.$icon_name.'.svg'); ?>
                    </div>
                <?php endif; ?>

                <div class="indent-block-content">
                    <?php the_sub_field('sub_field_indent_content'); ?>
                    <InnerBlocks />
                </div>
            </div>
            <?php
            $indent_block_iter++;
        endwhile;
        ?>
    </section>
<?php
endif;
